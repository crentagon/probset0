class Cross extends TurtleGraphicsWindow {

	public void myTurtleCmds() {
		forward(100);
		back(200);
		forward(100);
		right(90);
		forward(0);
		back(200);
	}

	public void drawSquare(int length) {
		forward(length);
		right(90);
		forward(length);
		right(90);
		forward(length);
		right(90);
		forward(length);
		right(90);

	}

	public void drawCircle(int radius) {
		pu();
		double degrees = 0;
		
		for(; degrees <= 360; degrees+=0.1){
			setxy((int)(radius*Math.cos(Math.toRadians(degrees))), (int)(radius*Math.sin(Math.toRadians(degrees))));		
			pd();
		}
	}

	// program starts here
	public static void main(String[] args) {
		Cross obj = new Cross();
		// obj.myTurtleCmds( );
		obj.drawSquare(100);
		obj.drawCircle(100);
	}

} // end class Cross

